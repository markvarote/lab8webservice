package th.ac.tu.siit.lab8webservice;

import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String, String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;

	String lastProvince = "bangkok";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Do not allow changing device orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

		File infile = getBaseContext().getFileStreamPath("province.txt");
		if (infile.exists()) {
			try {
				Scanner sc = new Scanner(infile);
				while (sc.hasNextLine()) {
					String line = sc.nextLine();
					lastProvince=line;
				}
				sc.close();
			} catch (FileNotFoundException e) {
			}
		}

		list = new ArrayList<Map<String, String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, new String[] {
				"name", "value" }, new int[] { R.id.tvName, R.id.tvValue });
		setListAdapter(adapter);
	}

	@Override
	protected void onStart() {
		super.onStart();

		refresh(5, lastProvince);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:

			refresh(3, lastProvince);
			break;
		case R.id.action_nonthaburi:

			refresh(0, "nonthaburi");
			break;
		case R.id.action_bangkok:

			refresh(0, "bangkok");
			break;
		case R.id.action_phatum:

			refresh(0, "pathum thani");
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void refresh(int time, String see) {
		lastProvince = see;
		try {
			FileOutputStream outfile = openFileOutput("province.txt",
					MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
			p.write(lastProvince);
			p.flush();
			p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data",
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data",
					Toast.LENGTH_SHORT);
			t.show();
		}
		// Check if the device is connected to a network
		ConnectivityManager mgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			// load the data from the web server when it is connected
			long current = System.currentTimeMillis();
			// load only when the time between now and the last update > 5
			// minutes
			if (current - lastUpdate > time * 1000) {
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/" + see + ".json");
			}
		} else {
			Toast t = Toast
					.makeText(
							this,
							"No Internet Connectivity on this device. Check your setting",
							Toast.LENGTH_LONG);
			t.show();
		}

	}

	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String, String> record;
		ProgressDialog dialog;

		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}

		// Executed under UI thread
		// Call before the task starts
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		// Executed under UI thread
		// Called after the task finished
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), result,
					Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			// Update the title bar of the activity
			setTitle(lastProvince + " Weather");
		}

		// Executed under Backgroud thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				// Use the first parameter as a URL
				URL url = new URL(params[0]);
				// Connect to the URL
				HttpURLConnection http = (HttpURLConnection) url
						.openConnection();
				http.setReadTimeout(10000);// 10s
				http.setConnectTimeout(15000);// 15s
				http.setRequestMethod("GET");
				// Read data from the URL
				http.setDoInput(true);
				http.connect();

				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(
							http.getInputStream()));
					while ((line = in.readLine()) != null) {
						buffer.append(line);
					}
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String, String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp") - 273.15;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", temp));
					list.add(record);

					// "weather" :[{"description":"....",}],
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String desc = w0.getString("description");
					record = new HashMap<String, String>();
					record.put("name", "Description");
					record.put("value", desc);
					list.add(record);

					// "main" :[{"pressure":"....",}],

					record = new HashMap<String, String>();
					record.put("name", "Pressure");
					int pressure = jmain.getInt("pressure");
					record.put("value", String.format(Locale.getDefault(),
							"%d hPa", pressure));
					list.add(record);

					record = new HashMap<String, String>();
					record.put("name", "Humidity");
					int humidity = jmain.getInt("humidity");
					record.put("value", String.format(Locale.getDefault(),
							"%d%%", humidity));
					list.add(record);

					record = new HashMap<String, String>();
					record.put("name", "temp_min");
					double tempmin = jmain.getDouble("temp_min") - 273.15;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", tempmin));
					list.add(record);

					record = new HashMap<String, String>();
					record.put("name", "temp_max");
					double tempmax = jmain.getDouble("temp_max") - 273.15;
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree celsius", tempmax));
					list.add(record);

					// "wind" :[{"speed":"....",}],
					JSONObject jwind = new JSONObject(buffer.toString());
					JSONObject wind = jwind.getJSONObject("wind");
					record = new HashMap<String, String>();
					record.put("name", "Wind Speed");
					double windSpeed = wind.getDouble("speed");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f mps", windSpeed));
					list.add(record);

					record = new HashMap<String, String>();
					record.put("name", "Wind Degree");
					double windDeg = wind.getDouble("deg");
					record.put("value", String.format(Locale.getDefault(),
							"%.1f degree", windDeg));
					list.add(record);

					return "Finished Loading Weather Data";
				} else {
					return "Error " + response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}
		}
	}

}
